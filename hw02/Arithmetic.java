///////
//// CSE2 Arithmetic
/// Emily Baker 9/11/18
///This proogram will find the total cost of each type of item before tax and the tax charged.
/// The program will also find the total cost of all items, the total tax charged on all items, and the total cost of the purchase including tax.
public class Arithmetic{
  public static void main(String args[]){
     //Number of pairs of pants
     int numPants = 3;
     //Cost per pair of pants
     double pantsPrice = 34.98;

     //Number of sweatshirts
     int numShirts = 2;
     //Cost per shirt
     double shirtPrice = 24.99;

     //Number of belts
     int numBelts = 1;
     //cost per belt
     double beltCost = 33.99;

     //the tax rate
     double paSalesTax = 0.06;
    
    //variable for the cost of pants
    double totalCostOfPants;
    
    //variable for the cost of sweatshirts
    double totalCostOfSweatshirts;
      
    //variable for the cost of belts
    double totalCostOfBelts;
      
    //variable for the sales tax charged on pants
    double salesTaxPants;
      
    //variable for the sales tax charged on sweatshirts
    double salesTaxShirts;
      
    //variable for the sales tax charged on belts
    double salesTaxBelts;
      
    //variable for the Total cost of purchase before tax
    double totalCostOfPurchaseBeforeTax;
      
    //variable for the total sales tax
    double totalSalesTax;
      
    //variable for the total paid for this transaction,including tax
    double totalIncludingTax;
      
    //the total cost of the pants= the number of pants multiplied by the price of a single pair of pants
    //to truncate the price to two decimal places, we first multiply the total cost by 100
    //we then convert the double to an integer, which loses all numbers beyond the deimcal point
    //then we convert our cost back to a double and divide the price by 100
    totalCostOfPants= (numPants*pantsPrice);
    totalCostOfPants = totalCostOfPants*100;
    totalCostOfPants = (int) totalCostOfPants;
    totalCostOfPants= (double) totalCostOfPants/100;
      
    //the total cost of the sweatshirts=the number of sweatshirts multiplied by the price of a single sweatshirt
    //to truncate the price to two decimal places, we first multiply the total cost by 100
    //we then convert the double to an integer, which loses all numbers beyond the deimcal point
    //then we convert our cost back to a double and divide the price by 100
    totalCostOfSweatshirts= (numShirts*shirtPrice);
    totalCostOfSweatshirts= totalCostOfSweatshirts*100;
    totalCostOfSweatshirts= (int) totalCostOfSweatshirts;
    totalCostOfSweatshirts= (double) totalCostOfSweatshirts/100;
      
    //the total cost of the belts=the number of belts multiplied by the price of a single belt
    //to truncate the price to two decimal places, we first multiply the total cost by 100
    //we then convert the double to an integer, which loses all numbers beyond the deimcal point
    //then we convert our cost back to a double and divide the price by 100
    totalCostOfBelts= (numBelts*beltCost);
    totalCostOfBelts= totalCostOfBelts*100;
    totalCostOfBelts= (int) totalCostOfBelts;
    totalCostOfBelts= (double) totalCostOfBelts/100;
      
    //the sales tax charged for the pants=the total price of pants multiplied the rate of tax in PA
    //to truncate the tax to two decimal places, we first multiply the total tax by 100
    //we then convert the double to an integer, which loses all numbers beyond the deimcal point
    //then we convert our tax back to a double and divide the tax by 100
    salesTaxPants= (totalCostOfPants*paSalesTax);
    salesTaxPants= salesTaxPants*100;
    salesTaxPants= (int) salesTaxPants;
    salesTaxPants= (double) salesTaxPants/100;
      
    //the sales tax charged for the sweatshirts=the total price of sweatshirts multiplied the rate of tax in PA
    //to truncate the tax to two decimal places, we first multiply the total tax by 100
    //we then convert the double to an integer, which loses all numbers beyond the deimcal point
    //then we convert our tax back to a double and divide the tax by 100
    salesTaxShirts= (totalCostOfSweatshirts*paSalesTax);
    salesTaxShirts= salesTaxShirts*100;
    salesTaxShirts= (int) salesTaxShirts;
    salesTaxShirts= (double) (salesTaxShirts)/100;
      
    //the sales tax charged for the belts=the total price of belts multiplied the rate of tax in PA
    //to truncate the tax to two decimal places, we first multiply the total tax by 100
    //we then convert the double to an integer, which loses all numbers beyond the deimcal point
    //then we convert our tax back to a double and divide the tax by 100
    salesTaxBelts= (totalCostOfBelts*paSalesTax);
    salesTaxBelts= salesTaxBelts*100;
    salesTaxBelts= (int) salesTaxBelts;
    salesTaxBelts= (double) (salesTaxBelts)/100;
      
    //the total cost of all purchases (before sales tax)= the total cost of pants+the total cost of sweathshirts+ the total cost of belts
    //to truncate the price to two decimal places, we first multiply the total cost by 100
    //we then convert the double to an integer, which loses all numbers beyond the deimcal point
    //then we convert our cost back to a double and divide the price by 100
    totalCostOfPurchaseBeforeTax= totalCostOfPants+totalCostOfSweatshirts+totalCostOfBelts;
    totalCostOfPurchaseBeforeTax= totalCostOfPurchaseBeforeTax*100;
    totalCostOfPurchaseBeforeTax= (int) totalCostOfPurchaseBeforeTax;
    totalCostOfPurchaseBeforeTax= (double) totalCostOfPurchaseBeforeTax/100;
      
    //the total sales tax on all purchases=the sales tax on pants+sales tax on sweatshirts+ sales tax on belts
    //to truncate the tax to two decimal places, we first multiply the total tax by 100
    //we then convert the double to an integer, which loses all numbers beyond the deimcal point
    //then we convert our tax back to a double and divide the tax by 100
    totalSalesTax= (salesTaxPants+salesTaxShirts+salesTaxBelts);
    totalSalesTax= totalSalesTax*100;
    totalSalesTax= (int) totalSalesTax; 
    totalSalesTax= (double) totalSalesTax/100;
      
    //the total amount paid for this transaction is equal to the total cost of the purchase before tax+ the total sales tax
    //to truncate the price to two decimal places, we first multiply the total cost by 100
    //we then convert the double to an integer, which loses all numbers beyond the deimcal point
    //then we convert our cost back to a double and divide the price by 100
    totalIncludingTax= (totalCostOfPurchaseBeforeTax+totalSalesTax);
    totalIncludingTax= totalIncludingTax*100;
    totalIncludingTax= (int) totalIncludingTax;
    totalIncludingTax= (double) totalIncludingTax/100;
      
    
      
     //Print out the data
     System.out.println("The total cost of the pants before tax is "+totalCostOfPants+".");
     System.out.println("The total sales tax on the pants is "+salesTaxPants+".");
     System.out.println("The total cost of the sweatshirts before tax is "+totalCostOfSweatshirts+".");
     System.out.println("The total sales tax on the sweatshirts is "+salesTaxShirts+".");
     System.out.println("The total cost of the belts before tax is "+totalCostOfBelts+".");
     System.out.println("The total sales tax on the belts is "+salesTaxBelts+"."); 
     System.out.println("The total cost of all purchases before tax is "+totalCostOfPurchaseBeforeTax+".");
     System.out.println("The total sales tax charged on all items is "+totalSalesTax+".");
     System.out.println("The total cost of all purchases, including sales tax, is "+totalIncludingTax+".");  
      
  }
}
                        
  
   
    
     
     
   
      
