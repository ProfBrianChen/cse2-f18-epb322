//Emily Baker CSE2 11/6/18
//this program will ask the user for a sample text, return that text and a menu asking the user what to do next.
//it will then perform a task based on what the user inputs 

import java.util.Scanner; //import the scanner
// 
//
public class WordTools{ //the class
    
  
    public static void main(String[] args) { //the main method 
      String textEntered = ""; //define the variable text entered 
      
       
      char optionChoice = 'a'; //define the character optionChoie
      sampleText();
      while(optionChoice != 'q'){ //the program will continue to print out the menu until the user tells it to quit
        printMenu();
        optionChoice = printMenu();
          switch (optionChoice){
            case 'c':
            System.out.println("There are " + getNumOfNonWSCharacters(textEntered) + " non white space characters."); //if c is selected, print out the number of non white space characters
            break;
            case 'w':
            System.out.println("There are " + getNumOfWords(textEntered) + " words."); //if w is selected, print out the number of words
            break;
       
        
          }
      }
    }
    
  
    //ask the user for a sample text and then return it back 
    public static String  sampleText() {
      String textEntered = "";
      Scanner myScanner = new Scanner( System.in ); //the scanner allows the user to input info
      System.out.print("Enter a sample text: "); //prompts the user to enter the dimensions
      textEntered = myScanner.next();
      System.out.println("You entered: " + textEntered);
      return textEntered;
      
    }
  
    //print out the menu and ask the user to choose an option 
    //return the choice as a character
    public static char printMenu() {
       
      Scanner scanner1 = new Scanner( System.in ); //the scanner allows the user to input info
      System.out.print("Menu \n c - Number of non-whitespace characters \n w - Number of words \n f - Find text \n r - Replace all !'s \n s - Shorten spaces \n q - Quit");
      System.out.print(" \n Choose an option: ");
      char choicee = scanner1.next().charAt(0);;
      return choicee; 
    }
  
  // get the number of non white space characters by going through each character and counting is as long as the character is not a spce
  //return the number of non white space characters as an integer
   public static int getNumOfNonWSCharacters(String userText){
      int numNWSChar = 0;
      int i = 0;
      for(i=0; i < (userText.length() - 1); i++){
        if( userText.charAt(i) != ' '){
          numNWSChar += numNWSChar;
        }
      }
      return numNWSChar;
    }
  
    //get the number of words by going through each character and counting the number of spaces
  //the number of words will be the number of spaces plus 1
  //return the number of words as an integer 
    public static int getNumOfWords(String userText){
      int numSpaces = 0;
      int i = 0;
      int numWords = 0; 
      for(i=0; i < (userText.length() - 1); i++){
        if( userText.charAt(i) == ' '){
          numSpaces += numSpaces;
        }
      numWords = (numSpaces + 1);
      
     }
    return numWords;
    }
      
   
  }

