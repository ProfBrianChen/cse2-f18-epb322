//Emily Baker CSE2 9/25/18
//This program will either take 2 numbers that the user inputs or generate two random numbers
//The program will then determine the slang terminology associated with the numbers and print it out
import java.util.Scanner;
// 
//
public class Hw05{
    			// main method required for every Java program
   			public static void main(String[] args) {
          
          Scanner myScanner = new Scanner( System.in ); //the scanner allows the user to input info
          System.out.print("How many hands would you like to be generated?"); //prompts the user to put in how many hands they want generated
          double numberOfHands = myScanner.nextDouble(); //assigns variable numberOfHands to the number of hands the user wants generated
          
          //declaring variables we will use later
          String suit1 = "";
          String suit2 = "";
          String suit3 = "";
          String suit4 = "";
          String suit5 = "";
          double counter = 0;
          double onePair = 0;
          double twoPair = 0;
          double threeOfAKind = 0;
          double fourOfAKind = 0; 
          while (counter < numberOfHands){ //for everyhand, this will happed
            int ranNum1 = (int)(Math.random()*(52))+1; //generate 5 random numbers, this represents each card
            int ranNum2 = (int)(Math.random()*(52))+1;
            int ranNum3 = (int)(Math.random()*(52))+1;
            int ranNum4 = (int)(Math.random()*(52))+1;
            int ranNum5 = (int)(Math.random()*(52))+1;
            String pokerHand = "";
            counter ++; 
            
           //assign a suit to each card
            if(1 <= ranNum1 && ranNum1 <= 13){ //if the number generated is between 1 and 13, this will associate with the diamond suit
             suit1 = "Diamonds";
            }
      
            else if(14 <= ranNum1 && ranNum1 <= 26){ //if the number generated is between 14 and 26, this will associate with the clubs suit
             suit1 = "Clubs";
            }
      
            else if(27 <= ranNum1 && ranNum1 <= 39){ //if the number generated is between 27 and 39, this will associate with the hearts suit
             suit1 = "Hearts";
            }
      
            else{ //otherwise, this will be the spades suit
             suit1 = "Spades";
            }
            if(1 <= ranNum2 && ranNum2 <= 13){ //if the number generated is between 1 and 13, this will associate with the diamond suit
             suit2 = "Diamonds";
            }
      
            else if(14 <= ranNum2 && ranNum2 <= 26){ //if the number generated is between 14 and 26, this will associate with the clubs suit
             suit2 = "Clubs";
            }
      
            else if(27 <= ranNum2 && ranNum2 <= 39){ //if the number generated is between 27 and 39, this will associate with the hearts suit
             suit2 = "Hearts";
            }
      
            else{ //otherwise, this will be the spades suit
             suit2 = "Spades";
            }
            if(1 <= ranNum3 && ranNum3 <= 13){ //if the number generated is between 1 and 13, this will associate with the diamond suit
             suit3 = "Diamonds";
            }
      
            else if(14 <= ranNum3 && ranNum3 <= 26){ //if the number generated is between 14 and 26, this will associate with the clubs suit
             suit3 = "Clubs";
            }
      
            else if(27 <= ranNum3 && ranNum3 <= 39){ //if the number generated is between 27 and 39, this will associate with the hearts suit
             suit3 = "Hearts";
            }
      
            else{ //otherwise, this will be the spades suit
             suit3 = "Spades";
            }
            if(1 <= ranNum4 && ranNum4 <= 13){ //if the number generated is between 1 and 13, this will associate with the diamond suit
             suit4 = "Diamonds";
            }
      
            else if(14 <= ranNum4 && ranNum4 <= 26){ //if the number generated is between 14 and 26, this will associate with the clubs suit
             suit4 = "Clubs";
            }
      
            else if(27 <= ranNum4 && ranNum4 <= 39){ //if the number generated is between 27 and 39, this will associate with the hearts suit
             suit4 = "Hearts";
            }
      
            else{ //otherwise, this will be the spades suit
             suit4 = "Spades";
            }
            if(1 <= ranNum5 && ranNum5 <= 13){ //if the number generated is between 1 and 13, this will associate with the diamond suit
             suit5 = "Diamonds";
            }
      
            else if(14 <= ranNum5 && ranNum5 <= 26){ //if the number generated is between 14 and 26, this will associate with the clubs suit
             suit5 = "Clubs";
            }
      
            else if(27 <= ranNum5 && ranNum5 <= 39){ //if the number generated is between 27 and 39, this will associate with the hearts suit
             suit5 = "Hearts";
            }
      
            else{ //otherwise, this will be the spades suit
             suit5 = "Spades";
            }
            
            
            //assign the face value to each card
            int cardNumber1 = ranNum1%13; //take the modulo of 13 of each number 
            String cardTypeDisplay1 = "";
            switch( cardNumber1 ){
              case 1: //if the remainder is one this will be an ace 
              cardTypeDisplay1 = "Ace";
              break;
              case 2: //if the remainder is 2-10, this will be the remainder
              cardTypeDisplay1 = "2";
              break;
              case 3:
              cardTypeDisplay1 = "3";
              break;
              case 4: 
              cardTypeDisplay1 = "4";
              break;
              case 5:
              cardTypeDisplay1 = "5";
              break;
              case 6:
              cardTypeDisplay1 = "6";
              break;
              case 7:
              cardTypeDisplay1 = "7";
              break;
              case 8:
              cardTypeDisplay1 = "8";
              break;
              case 9: 
              cardTypeDisplay1 = "9";
              break;
              case 10:
              cardTypeDisplay1 = "10";
              break;
              case 11: //if the remainder is 11 this will be a jack 
              cardTypeDisplay1 = "Jack";
              break;
              case 12: //if the remainder is 12 this will be a queen
              cardTypeDisplay1 = "Queen";
              break;
              case 0: //if the remainder is 0 this will be a king
              cardTypeDisplay1 = "King";
              break;
            }
            int cardNumber2 = ranNum2%13; //take the modulo of 13 of each number 
            String cardTypeDisplay2 = "";
            switch( cardNumber2 ){
              case 1: //if the remainder is one this will be an ace 
              cardTypeDisplay2 = "Ace";
              break;
              case 2: //if the remainder is 2-10, this will be the remainder
              cardTypeDisplay2 = "2";
              break;
              case 3:
              cardTypeDisplay2 = "3";
              break;
              case 4: 
              cardTypeDisplay2 = "4";
              break;
              case 5:
              cardTypeDisplay2 = "5";
              break;
              case 6:
              cardTypeDisplay2 = "6";
              break;
              case 7:
              cardTypeDisplay2 = "7";
              break;
              case 8:
              cardTypeDisplay2 = "8";
              break;
              case 9: 
              cardTypeDisplay2 = "9";
              break;
              case 10:
              cardTypeDisplay2 = "10";
              break;
              case 11: //if the remainder is 11 this will be a jack 
              cardTypeDisplay2 = "Jack";
              break;
              case 12: //if the remainder is 12 this will be a queen
              cardTypeDisplay2 = "Queen";
              break;
              case 0: //if the remainder is 0 this will be a king
              cardTypeDisplay2 = "King";
              break;
            }
            int cardNumber3 = ranNum3%13; //take the modulo of 13 of each number 
            String cardTypeDisplay3 = "";
            switch( cardNumber3 ){
              case 1: //if the remainder is one this will be an ace 
              cardTypeDisplay3 = "Ace";
              break;
              case 2: //if the remainder is 2-10, this will be the remainder
              cardTypeDisplay3 = "2";
              break;
              case 3:
              cardTypeDisplay3 = "3";
              break;
              case 4: 
              cardTypeDisplay3 = "4";
              break;
              case 5:
              cardTypeDisplay3 = "5";
              break;
              case 6:
              cardTypeDisplay3 = "6";
              break;
              case 7:
              cardTypeDisplay3 = "7";
              break;
              case 8:
              cardTypeDisplay3 = "8";
              break;
              case 9: 
              cardTypeDisplay3 = "9";
              break;
              case 10:
              cardTypeDisplay3 = "10";
              break;
              case 11: //if the remainder is 11 this will be a jack 
              cardTypeDisplay3 = "Jack";
              break;
              case 12: //if the remainder is 12 this will be a queen
              cardTypeDisplay3 = "Queen";
              break;
              case 0: //if the remainder is 0 this will be a king
              cardTypeDisplay3 = "King";
              break;
             } 
            int cardNumber4 = ranNum4%13; //take the modulo of 13 of each number 
            String cardTypeDisplay4 = "";
            switch( cardNumber4 ){
              case 1: //if the remainder is one this will be an ace 
              cardTypeDisplay4 = "Ace";
              break;
              case 2: //if the remainder is 2-10, this will be the remainder
              cardTypeDisplay4 = "2";
              break;
              case 3:
              cardTypeDisplay4 = "3";
              break;
              case 4: 
              cardTypeDisplay4 = "4";
              break;
              case 5:
              cardTypeDisplay4 = "5";
              break;
              case 6:
              cardTypeDisplay4 = "6";
              break;
              case 7:
              cardTypeDisplay4 = "7";
              break;
              case 8:
              cardTypeDisplay4 = "8";
              break;
              case 9: 
              cardTypeDisplay4 = "9";
              break;
              case 10:
              cardTypeDisplay4 = "10";
              break;
              case 11: //if the remainder is 11 this will be a jack 
              cardTypeDisplay4 = "Jack";
              break;
              case 12: //if the remainder is 12 this will be a queen
              cardTypeDisplay4 = "Queen";
              break;
              case 0: //if the remainder is 0 this will be a king
              cardTypeDisplay4 = "King";
              break;
             }
             int cardNumber5 = ranNum5%13; //take the modulo of 13 of each number 
             String cardTypeDisplay5 = "";
             switch( cardNumber5 ){
              case 1: //if the remainder is one this will be an ace 
              cardTypeDisplay5 = "Ace";
              break;
              case 2: //if the remainder is 2-10, this will be the remainder
              cardTypeDisplay5 = "2";
              break;
              case 3:
              cardTypeDisplay5 = "3";
              break;
              case 4: 
              cardTypeDisplay5 = "4";
              break;
              case 5:
              cardTypeDisplay5 = "5";
              break;
              case 6:
              cardTypeDisplay5 = "6";
              break;
              case 7:
              cardTypeDisplay5 = "7";
              break;
              case 8:
              cardTypeDisplay5 = "8";
              break;
              case 9: 
              cardTypeDisplay5 = "9";
              break;
              case 10:
              cardTypeDisplay5 = "10";
              break;
              case 11: //if the remainder is 11 this will be a jack 
              cardTypeDisplay5 = "Jack";
              break;
              case 12: //if the remainder is 12 this will be a queen
              cardTypeDisplay5= "Queen";
              break;
              case 0: //if the remainder is 0 this will be a king
              cardTypeDisplay5 = "King";
              break;
             } 
            
            
            //the next chunk of code reads if any cards have the same face value, and records if it is a four of a kind, three of a kind, two pair or one pair
            if(((cardNumber1) == (cardNumber2)) && ((cardNumber2) == (cardNumber3)) && ((cardNumber3) == (cardNumber4))){
              pokerHand = "Four of a kind";
            }
            else if(((cardNumber2) == (cardNumber3)) && ((cardNumber3) == (cardNumber4)) && ((cardNumber4) == (cardNumber5))){
              pokerHand = "Four of a kind";
            }
            else if(((cardNumber3) == (cardNumber4)) && ((cardNumber4) == (cardNumber5)) && ((cardNumber5) == (cardNumber1))){
              pokerHand = "Four of a kind";
            }
            else if(((cardNumber4) == (cardNumber5)) && ((cardNumber5) == (cardNumber1)) && ((cardNumber1) == (cardNumber2))){
              pokerHand = "Four of a kind";
            }
            else if(((cardNumber5) == (cardNumber1)) && ((cardNumber1) == (cardNumber2)) && ((cardNumber2) == (cardNumber3))){
              pokerHand = "Four of a kind";
            }
            else if(((cardNumber1) == (cardNumber2)) && ((cardNumber2) == (cardNumber3))){
              pokerHand = "Three of a kind";
            }
            else if(((cardNumber2) == (cardNumber3)) && ((cardNumber3) == (cardNumber4))){
               pokerHand = "Three of a kind";
            }
            else if(((cardNumber3) == (cardNumber4)) && ((cardNumber4) == (cardNumber5))){
              pokerHand = "Three of a kind";
            }
            else if(((cardNumber4) == (cardNumber5)) && ((cardNumber5) == (cardNumber1))){
              pokerHand = "Three of a kind";
            }
            else if(((cardNumber5) == (cardNumber1)) && ((cardNumber1) == (cardNumber2))){
              pokerHand = "Three of a kind";
            }
            else if(((cardNumber1) == (cardNumber2)) && ((cardNumber3) == (cardNumber4))){
              pokerHand = "Two pair";
            }
            else if(((cardNumber2) == (cardNumber3)) && ((cardNumber4) == (cardNumber5))){
              pokerHand = "Two pair";
            }
            else if(((cardNumber3) == (cardNumber4)) && ((cardNumber5) == (cardNumber1))){
              pokerHand = "Two pair";
            }
            else if(((cardNumber4) == (cardNumber5)) && ((cardNumber1) == (cardNumber2))){
              pokerHand = "Two pair";
            }
            else if(((cardNumber5) == (cardNumber1)) && ((cardNumber2) == (cardNumber3))){
              pokerHand = "Two pair";
            }  
            else if(((cardNumber1) == (cardNumber2))){
              pokerHand = "One-pair";
            }
            else if(((cardNumber2) == (cardNumber3))){
              pokerHand = "One-pair";
            }
            else if(((cardNumber3) == (cardNumber4))){
              pokerHand = "One-pair";
            }
            else if(((cardNumber4) == (cardNumber5))){
              pokerHand = "One-pair";
            }
            else if(((cardNumber5) == (cardNumber1))){
              pokerHand = "One-pair";
            }
     
            //counts how many of each type their are
            if(pokerHand == "One-pair"){
              onePair ++;
            }
            else if(pokerHand == "Two pair"){
              twoPair ++;
           }
            else if(pokerHand == "Three of a kind"){
              threeOfAKind ++;
           }
            else if(pokerHand == "Four of a kind"){
              fourOfAKind ++;
           }
      
       }
     
      //divides the number of each type by the number of hands to get the probability
     double fourKindChance = ((double)(fourOfAKind)/numberOfHands);
     double threeKindChance = ((double)(threeOfAKind)/numberOfHands);
     double twoPairChance = ((double)(twoPair)/numberOfHands);
     //prints out the probabilities
     System.out.println("The number of loops: " + numberOfHands);
     System.out.println("The probability of Four-of-a-kind: " + fourKindChance);
     System.out.println("The probability of Three-of-a-kind: " + (threeOfAKind/numberOfHands));
     System.out.println("The probabiliy of Two-pair: " + (twoPair/numberOfHands));
     System.out.println("The probability of One-pair: " + (onePair/numberOfHands));
     
     }
  }
     
        
   