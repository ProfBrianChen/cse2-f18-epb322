//Emily Baker CSE2 10/4/2018
//This program will ask users to enter information relating to a course they are currently taking - ask for the course number, department name, the number of times it meets in a week, the time the class starts, the instructor name, and the number of students.
// It will then check to make sure that what the user enters is actually of the correct type, and if not, use an infinite loop to ask again.
import java.util.Scanner;
// 
//
public class ClassInfo{
    			// main method required for every Java program
   			public static void main(String[] args) {
          int courseNumber = 0;
          Scanner myScanner = new Scanner( System.in ); //the scanner allows the user to input info
          System.out.print("What is your course number?");
          while (){
            boolean correct = myScanner.hasNextInt();
            if (correct == true){
              courseNumber = myScanner.nextInt();
            }
           else{
              myScanner.next();
            }
          }
        
        }
}
          