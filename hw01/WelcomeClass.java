//////////////
//// CSE 02 Hello World
///
public class WelcomeClass{
  
  public static void main(String args[]){
    ///prints welcome message to terminal window
    System.out.println("  -----------");
    System.out.println("  | WELCOME |");
    System.out.println("  -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-E--P--B--3--2--2->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("   v  v  v  v  v  v");
    System.out.println("My name is Emily, I'm from Chicago and I'm in the college of arts and sciences.");
    
  }
  
}