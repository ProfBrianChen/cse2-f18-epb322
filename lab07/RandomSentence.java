//Emily Baker CSE2 10/22/18
//This program will print a random sentence, and keep printing more until the user says they don't want any more. It will then print out a random paragraph. 

import java.util.Random;
import java.util.Scanner;
public class RandomSentence{
    			// main method required for every Java program
   			public static void main(String[] args) { //this method prints a random sentence then asks the user if they'd like another
          //it keeps repeating that until the user says no. it then prints out the storyline
          
          Random randomGenerator = new Random();
          System.out.print("The" + adj() + adj() + subj() + verb() + "the" + adj() + obj());
          System.out.print("\n");
          Scanner myScanner = new Scanner( System.in ); //the scanner allows the user to input info
          System.out.print("Would you like another sentence? "); //prompts the user to enter the dimensions
          String userAnswer = myScanner.next(); //assigns the variable dimenstions to the dim
          while( userAnswer.equals("yes")){
              System.out.print("The" + adj() + adj() + subj() + verb() + "the" + adj() + obj());
              System.out.print("\n");
              Scanner myScanner1 = new Scanner( System.in ); 
              System.out.print("Would you like another sentence? ");
              userAnswer = myScanner.next(); 
            
         
          }
         System.out.println( thesis() + actionSentence() + actionSentence() + actionSentence() + closingSentence()); 
          
          
          
        }
  
       
  
        public static String thesis() { //this is the first sentence of the story line 
          return( "The " + adj() + adj() + subj() + verb() + "the" + adj() + obj() + ".");
        }
        
        public static String actionSentence() { //this is the action part of the storyline 
          return( "This " + subj() + " was " + adj() + " to the " + obj() + ".");
          
        }
  
        public static String closingSentence() { //this is the last sentence of the storyline
          return( "That " + subj() + " knew her " + obj() + "!");
        }
  
        public static String adj() { //this generates a random adjective
          Random randomGenerator = new Random();
          int randomInt1 = randomGenerator.nextInt(10);
          String adjective = "";
          //This statement generates random integers less than 10
          switch( randomInt1){
            case 0:
            adjective = " Quick ";
            break;
            case 1:
            adjective = " Tiny ";
            break;
            case 2:
            adjective = " Big ";
            break;
            case 3:
            adjective = " Loud ";
            break;
            case 4:
            adjective = " Lazy ";
            break;
            case 5:
            adjective = " Happy ";
            break;
            case 6:
            adjective = " Charming ";
            break;
            case 7:
            adjective = " Ugly ";
            break;
            case 8:
            adjective = " Beautiful ";
            break;
            case 9:
            adjective = " Tired ";
            break;
          }
          return(adjective); 
          
        }
          
        public static String subj() { //this generates a random subject
          Random randomGenerator = new Random();
          int randomInt2 = randomGenerator.nextInt(10);
          String subject = "";
          //This statement generates random integers less than 10
          switch( randomInt2){
            case 0:
            subject = " Girl ";
            break;
            case 1:
            subject = " Boy ";
            break;
            case 2:
            subject = " Giraffe ";
            break;
            case 3:
            subject = " Dog ";
            break;
            case 4:
            subject = " Cat ";
            break;
            case 5:
            subject = " Alien ";
            break;
            case 6:
            subject = " Chicken ";
            break;
            case 7:
            subject = " Pig ";
            break;
            case 8:
            subject = " Rat ";
            break;
            case 9:
            subject = " Woman ";
            break;
          }
          return(subject);  
        }
          
        public static String verb() { //this generates a random verb 
          Random randomGenerator = new Random();
          int randomInt3 = randomGenerator.nextInt(10);
          String verbb = "";
          //This statement generates random integers less than 10
          switch( randomInt3){
            case 0:
            verbb = " waved at ";
            break;
            case 1:
            verbb = " jumped over ";
            break;
            case 2:
            verbb = " sang to ";
            break;
            case 3:
            verbb = " listened to ";
            break;
            case 4:
            verbb = " ate ";
            break;
            case 5:
            verbb = " played with ";
            break;
            case 6:
            verbb = " walked with ";
            break;
            case 7:
            verbb = " talked to ";
            break;
            case 8:
            verbb = " hugged ";
            break;
            case 9:
            verbb = " kissed ";
            break;
          }
          return(verbb);  
        }

         public static String obj() { //this generates a random object
          Random randomGenerator = new Random();
          int randomInt4 = randomGenerator.nextInt(10);
          String object = "";
          //This statement generates random integers less than 10
          switch( randomInt4){
            case 0:
            object = " lizard ";
            break;
            case 1:
            object = " chair ";
            break;
            case 2:
            object = " desk ";
            break;
            case 3:
            object = " bird ";
            break;
            case 4:
            object = " plate ";
            break;
            case 5:
            object = " elephant ";
            break;
            case 6:
            object = " parrot ";
            break;
            case 7:
            object = " computer ";
            break;
            case 8:
            object = " lion ";
            break;
            case 9:
            object = " tiger ";
            break;
          }
          return(object);  
        } 
}


          
