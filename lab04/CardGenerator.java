//Emily Baker CSE02 9/20/2018
//Card Generator
//This program will use a random number generator to choose a random card from a standard deck of cards. 
public class CardGenerator {
    	// main method required for every Java program
   	public static void main(String[] args) {
      // our input data (variables) are below
      int ranNum = (int)(Math.random()*(52))+1;  //generate a random number between 1 and 52
      String suit; //create the variable for the string suit
      //System.out.println(ranNum); 
      
      if(1 <= ranNum & ranNum <= 13){ //if the number generated is between 1 and 13, this will associate with the diamond suit
        suit = "Diamonds";
      }
      
      else if(14 <= ranNum & ranNum <= 26){ //if the number generated is between 14 and 26, this will associate with the clubs suit
        suit = "Clubs";
      }
      
      else if(27 <= ranNum & ranNum <= 39){ //if the number generated is between 27 and 39, this will associate with the hearts suit
        suit = "Hearts";
      }
      
      else{ //otherwise, this will be the spades suit
      suit = "Spades";
      }
      //System.out.println(suit); 
      
      int cardNumber = ranNum%13; //take the modulo of 13 of each number 
      String cardTypeDisplay = "";
      switch( cardNumber ){
        case 1: //if the remainder is one this will be an ace 
        cardTypeDisplay = "Ace";
        break;
        case 2: //if the remainder is 2-10, this will be the remainder
        cardTypeDisplay = "2";
        break;
        case 3:
        cardTypeDisplay = "3";
        break;
        case 4: 
        cardTypeDisplay = "4";
        break;
        case 5:
        cardTypeDisplay = "5";
        break;
        case 6:
        cardTypeDisplay = "6";
        break;
        case 7:
        cardTypeDisplay = "7";
        break;
        case 8:
        cardTypeDisplay = "8";
        break;
        case 9: 
        cardTypeDisplay = "9";
        break;
        case 10:
        cardTypeDisplay = "10";
        break;
        case 11: //if the remainder is 11 this will be a jack 
        cardTypeDisplay = "Jack";
        break;
        case 12: //if the remainder is 12 this will be a queen
        cardTypeDisplay = "Queen";
        break;
        case 0: //if the remainder is 0 this will be a king
        cardTypeDisplay = "King";
        break;
      }
        System.out.println("You picked the " + cardTypeDisplay + " of " + suit + "."); //print out the card type and suit
          
      
    }
}
      