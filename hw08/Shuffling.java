import java.util.Scanner; //import the scanner
public class Shuffling{ 
public static void main(String[] args) { 
Scanner scan = new Scanner(System.in); 
 //suits club, heart, spade or diamond 
 String[] suitNames={"C","H","S","D"};    //initialize the strings for the suit and rank 
String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
String[] cards = new String[52]; //make arrays for the suit and rank
String[] hand = new String[5]; 
int numCards = 5; 
int again = 1; 
int index = 51;
for (int i=0; i<52; i++){ //create the initial deck, assigning a each rank to each suit
  cards[i]=rankNames[i%13]+suitNames[i/13]; 
  System.out.print(cards[i]+" ");  //print out the original, ordered deck 
} 
System.out.println(); 

 
shuffle(cards); //call the method to shuffle the cards
System.out.print( "\n");
System.out.println("Shuffled: ");
printArray(cards); //call the method to print out the deck of shuffled cards 
System.out.println("\n");
System.out.println("Hand: ");
while(again == 1){ //pring out a hand. keep asking the user if theyd like another 
   
   hand = getHand(cards,index,numCards); 
   printArray(hand);
   index = index - numCards;
   System.out.println("Enter a 1 if you want another hand drawn"); 
   again = scan.nextInt(); 
}  
  } 

public static void printArray(String[] list){ //print out the array inputed 
  
for(int i = 0; i<list.length; i++){ //while there are elements left in the array, keep printing out the next element
  System.out.print(list[i] + " ");
}
}
  

  
public static void shuffle(String[] list){ //shuffle the cards 
  int i;
  int ranNum; 
  String temp = "";
  String[] suitNames={"C","H","S","D"};    
  String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
  String[] cards = new String[52]; 
  String[] hand = new String[5]; 
  int numCards = 0; 
  int again = 1; 
  int index = 0;

  for(i=0; i<52; i++){   //for every card in the deck, generate a random element to become the first element, and put the current first element in the random elements spot. 
    ranNum = (int)(Math.random()*(51)) + 1; //repeat this 52 times to create a good shuffle. 
    temp = list[0];
    list[0] = list[ranNum];
    list[ranNum] = temp;
  
  }


}
  
  
 
public static String[] getHand(String[] list,int index, int numCards){ //get a hand of 5 cards

  
  String[] hand = new String[numCards]; 
  for(int i = 0; i<numCards;i++){ //for the number of cards specified, hand out 5 from the top of the deck, which is the end of the array 
   
    hand[i] = list[index];
    if(index<0){ //if you get to the end of the deck, begin a new one
      index = 51;
    }
    else{
      index--; 
    }
  
}
return hand;  //return the array of the hand 

}
}

