//EMily Baker CSE2 11/15/2018
//this program takes an array and manipulates it using various methods 

public class ArraysAndMethods{ 

public static void main(String[] args) { 
 
 int[] array0 = new int[10]; //initialize all arrays that will be used
 int[] array1 = new int[array0.length];
 int[] array2 = new int[array1.length]; 
 int[] array3 = new int[array2.length];
 array0[0] = 1; //make the values of the array 
 array0[1] = 5;
 
 array0[2] = 3;
 array0[3] = 9;
 array0[4] = 2;
 array0[5] = 0;
 array0[6] = 3;
 array0[7] = 2;
 array0[8] = 7;
 array0[9] = 5;
 
 array1 = copy(array0); //array1 is a copy of array0
 array2 = copy(array1); //array2 is a copy of array1
 inverter(array0); //run method inverter on array0
 print(array0); //run method print on array0
 inverter2(array1); //run method inverter2 on array1
 System.out.println("\n");
 print(array1); //run method print on array1
 array3 = inverter2(array2); //run method inverter2 on array2 and assign it to array3
 System.out.println("\n");
 print(array3); //run method print on array3
}
  
public static int[] copy(int[] originalArray) { //create a copy of the input 
  int[] newArray = new int[originalArray.length]; 
  for(int i = 0; i<originalArray.length; i++){ //assign every element of the inputed array to the corresponding element of the new array 
    newArray[i] = originalArray[i];
  }
  return newArray; 
}
  
public static void inverter(int[] originalArray){
  int temp = 0;
  for (int i = 0; i<(originalArray.length)/2; i++){ //swap first element with last, second with second to last, etc
    temp = originalArray[i]; //only do this the number of times as half of the array so it doesnt swap back to the original position 
    originalArray[i] = originalArray[((originalArray.length-1)-i)];
    originalArray[((originalArray.length-1)-i)] = temp;
  }
}
                 
 

                
  
public static int[] inverter2(int[] originalArray){
  int[] newArray1 = new int[originalArray.length];
  int[] newArray2 = new int[originalArray.length];
  newArray1 = copy(originalArray);  //run the method copy on the original array 
  inverter(newArray1); //run the method inverter on the array from above 
  return newArray1; 
  
}
  
public static void print(int[] originalArray){
  for(int i = 0; i<originalArray.length; i++){ //while there are elements left in the array, keep printing out the next element
  System.out.print(originalArray[i] + " "); 
}
}
  
  
  
  
  
  
}