//Emily Baker CSE2 9/13/2018
//This program will divide a bill, including the tip among multiple people who wish to split the check evenly
import java.util.Scanner;
// 
//
public class Check{
    			// main method required for every Java program
   			public static void main(String[] args) {
          
          Scanner myScanner = new Scanner( System.in ); //the scanner allows the user to input info
          System.out.print("Enter the original cost of the check in the form xx.xx: "); //this prints out a "Enter the original cost of the check in the form xx.xx: " which prompts the user to enter the cost of the bill 
          double checkCost = myScanner.nextDouble(); //this creates a variable for the cost of the check, called checkCost
          System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): " ); //this prompts the user to enter the percentage of tip they would like to include
          double tipPercent = myScanner.nextDouble(); //this creates a variable for the percent of the tip, called tip percent
          tipPercent /= 100; //We want to convert the percentage into a decimal value
          System.out.print("Enter the number of people who went out to dinner: "); //this prompts the user to enter the number of people who will be splitting the cost
          int numPeople = myScanner.nextInt(); //this creates a variable for the number of people splitting the cost
          double totalCost; //this is the variable for the cost
          double costPerPerson; //this is the variable for the cost per person
          int dollars,   //whole dollar amount of cost 
          dimes, pennies; //for storing digits
          //to the right of the decimal point 
          //for the cost$ 
          totalCost = checkCost * (1 + tipPercent); //this calculates the cost of the bill, including tip 
          costPerPerson = totalCost / numPeople; //this divides the total cost by the number of people to find the cost per person
          //get the whole amount, dropping decimal fraction
          dollars=(int)costPerPerson;
          //get dimes amount, e.g., 
          // (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
          //  where the % (mod) operator returns the remainder
          //  after the division:   583%100 -> 83, 27%5 -> 2 
          dimes=(int)(costPerPerson * 10) % 10; //the tens place of the cost
          pennies=(int)(costPerPerson * 100) % 10; //the ones place of the cost
          System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies); //outputs the amount each person must pay 

}  //end of main method   
  	} //end of class





