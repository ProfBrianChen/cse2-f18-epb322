//Emily Baker CSE2 10/22/18
//This program will take the integer that the user inputs and create an x inside of a square of the dimensions the user chose

import java.util.Scanner;
// 
//
public class EncryptedX{
    			// main method required for every Java program
   			public static void main(String[] args) {
          
          Scanner myScanner = new Scanner( System.in ); //the scanner allows the user to input info
          System.out.print("Enter an integer 1-100.  "); //prompts the user to enter the dimensions
          double dimensions = myScanner.nextDouble(); //assigns the variable dimenstions to the dimension
          
          while( !myScanner.hasNextInt() ){ //makes sure the user enters an integer. if not, it tells the user to enter an integer
            System.out.print("Enter an integer 1-100. ");
            myScanner.next();
          }
          
         
          int dimension = myScanner.nextInt(); //variable for dimension
          int d = dimension;
          if( dimension < 0 ){ 
            d = -dimension; 
          }
          
          //creates the hidden x by placing a "#" in all spots except where i=j or i is the inverse of j
          for( int i = 0; i<d; i++){ 
	          for( int j = 0; j<d; j++ ){
		          if (i == j || i == (d-1) - j ){
			          System.out.print(" ");
		          }
		          else{
			          System.out.print("#");
		          }
	        
        }    // starts new line 
            System.out.print("\n");

        }
}
}

          
          