// Emily Baker
// 9/6/2018
// CSE2
// This program will take the data from the cyclometer (time elapsed in seconds, and the number of rotations of the front wheel during that time)
// For two trips, this program will print the number of minutes for each trip, the number of counts for each trip, the distance of each trip in miles, and the distance for the two trips combined
public class Cyclometer {
    	// main method required for every Java program
   	public static void main(String[] args) {
      // our input data (variables) are below 

	   	int secsTrip1=480;  // This is the time elapsed in seconds for trip 1
      int secsTrip2=3220;  // This is the time elapsed in seconds for trip 2
		  int countsTrip1=1561;  // This is the number of rotations of the front wheel from trip 1
		  int countsTrip2=9037; // This is the number of rotations of the front wheel from trip 2
      // the variables below will be used to calculate our output data
      double wheelDiameter=27.0,  //this is the diameter of the wheel
  	  PI=3.14159, //this is pi, which we will need to calculate our output data
  	  feetPerMile=5280,  // this is the number of feet per mile which we will need for conversions
  	  inchesPerFoot=12,   //this is the number of inches per foot which we will need for conversions
  	  secondsPerMinute=60;  //this is the number of seconds per minute which we will need for conversions 
	    double distanceTrip1, distanceTrip2,totalDistance;  //this is the total distance we would have traveled from both trips
      // the code below will print out the time taken and number of counts for each trip
      System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts.");
	    System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts.");
      //the calculations are being run below
	    distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches (from trip 1)
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
	    distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
	    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; // this gives the distance traveled (from trip 2) in miles
	    totalDistance=distanceTrip1+distanceTrip2; // this totals the distances from each trip to get the total distance
      //Print out the output data.
      System.out.println("Trip 1 was "+distanceTrip1+" miles");
	    System.out.println("Trip 2 was "+distanceTrip2+" miles");
	    System.out.println("The total distance was "+totalDistance+" miles");



	}  //end of main method   
} //end of class
