//11/27/2018 CSE2
//this code will perform different functions on a randomized array 
import java.util.Scanner;
public class RemoveElements{
  public static void main(String [] arg){
	Scanner scan=new Scanner(System.in);
int num[]=new int[10];
int newArray1[];
int newArray2[];
int index,target;
	String answer="";
	do{
  	System.out.print("Random input 10 ints [0-9]"); //create a random array of 10 integers of numbers 0-9 
  	num = randomInput();
  	String out = "The original array is:"; //print out the randomly generated array 
  	out += listArray(num);
  	System.out.println(out);
 
  	System.out.print("Enter the index ");
  	index = scan.nextInt();
  	newArray1 = delete(num,index);
  	String out1="The output array is ";
  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1);
 
      System.out.print("Enter the target value ");
  	target = scan.nextInt();
  	newArray2 = remove(num,target);
  	String out2="The output array is ";
  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2);
  	 
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	answer=scan.next();
	}while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){ //this method returns the array in the form {1, 2, 3, ...}
	String out="{";
	for(int j=0;j<num.length;j++){
  	if(j>0){
    	out+=", ";
  	}
  	out+=num[j];
	}
	out+="} ";
	return out;
  }


public static int[] randomInput(){
  int[] randomArray = new int[10];
  int ranNum = 0; 
  for(int i = 0; i<10; i ++){ //repeat this ten times
    ranNum = (int)(Math.random()*(9)); //generate a random number 0-9 and make that the i'th element of the array 
    randomArray[i] = ranNum;
  }
  return randomArray;
}
  
public static int[] delete(int[] list,int pos){
  int[] newArray = new int[list.length - 1];
  
  int i = 0;
  int j = 0;
  while(i<newArray.length){
    if(i < pos){  //if that position is less than the position to be deleted, 
      newArray[j] = list[i]; //that element of the new array will be the same as the corresponding element of the original array 
      i++;
      j++;
    }
    else if(i == pos){ //if the that position is the same as the position to be deleted, do not add anything to the new array
      i++; 
    }
    else if (i > pos){ //if that postition is less than the position to be deleted, 
      newArray[j] = list[i+1]; //that element of the new array will be the same as the corresponding next element of the originsl array 
      i++ ;
      j++;
    }
  }
 return newArray; //return the new array 
}
  
  
public static int[] remove(int[] list, int target){
int j = 0;
int i = 0;
int k = 0;
int p = 0;
while(i<list.length){
  if(list[i] == target){
    j++; //count how many times the target integer appears in the original array 
    i++;
  }
  else if(list[i] != target){
    i++;
  }
}
int[] updatedArray = new int[list.length - j]; //the length of the new array will be the length of the old array minus the number of times the target integer appears 
while(k<list.length){
  if(list[k] == target){ //if the element of the old array is the target, do not add this to the new array 
    k++;
  }
  else if(list[k] != target){ //if the element of the old array is not the target, add this to the new array 
    updatedArray[p] = list[k];
    p++;
    k++;
  }
}

  
return updatedArray; //return the new array 
}
  
  
  
}