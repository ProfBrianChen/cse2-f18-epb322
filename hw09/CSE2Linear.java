// cse2 11/27/2018
//this program will prompt the user to enter 15 integers to represent grades
// it will then check to ensure only integers were entered and that every integer is between 1-100, and that the integers are in order from least to greatest, then print the array
// it will then use binary search to find the grade that was searched for and print the number of iterations used
import java.util.Scanner; //import the scanner
import java.util.Random; //import the random 
public class CSE2Linear{ 

public static void main(String[] args) { 
  int[] grades = new int[15]; //initialize the array grades, which will store all of the students' grades 
  
  int i = 0;
  
  
  int gradeInputed = 0;
  while (i < 15){ //until you get 15 grades, keep asking for integers 
    int j = 0; 
   
    while( j == 0){
      Scanner scanner1 = new Scanner( System.in ); //you'll have to enter the number twice for it to work.. sorry:(
      System.out.println( " Enter a student's grade as an integer 0-100. ");
      gradeInputed = scanner1.nextInt();
      if (!scanner1.hasNextInt()){
        System.out.println(" This is not an integer. Enter an integer. "); //if the user does not enter an integer, ask for another
        break;
      }
       
      else if ( gradeInputed > 100 || gradeInputed < 0 ) {
        System.out.println( " Make sure the integer is 0-100. "); //if the user enters an integer over 100 or under 0, ask for another
        break;
      }
      else{
      grades[i] = gradeInputed; //put the grade inputed as the element of the array 
      }
    if( i == 0){
      
      j = 1; //exit the while loop and establish that grade as the first element of the array 
      i++;
    }
    else if (i != 0){
      if(grades[i] < grades[i-1]){ //if the new integer is smaller than the previous, ask the user to input another 
      System.out.println( "Make sure the grades are from least to greatest. ");
      break;
      }
      else if (grades[i] >= grades[i-1]){ //if the new integer is larger or equal to the last, exit the while loop and establish that grade as that number in the array 
      j = 1;
      i++; 
    } 
    }
  }
  
  }
  
  
 
  binarySearch(grades); //run the method binary search for the array grades 
  
  print(randomize(grades));  //run the method print on the method randomize on grades
  linearSearch(grades);  //run the method linearSearch on the array grades 
}
  
  public static void binarySearch(int[] arrayUsed){ //this is the method binarySearch
  int gradeSearched = 0;
  int found = 0; 
  Scanner scanner2 = new Scanner( System.in ); //establish the scanner 
  System.out.print(" Enter a grade to be searched for. "); //prompt the user to enter an integer to be searched for
  gradeSearched = scanner2.nextInt(); //assign the integer entered as gradeSearched
  int low = 0;
  int iterations = 0; 
  int high = arrayUsed.length - 1; //professor carr gave us this code in class 
  while(high >= low){ // keep running this loop until the number has been found or you have exhausted all numbers
    int mid = (low + high) /2; //make the mid the amidpoint of the high and low values of the places of the array 
    if (gradeSearched < arrayUsed[mid]) { //if the grade searched is less than the midpoint, the midpoint is now the high 
      high = mid -1;
    }
    else if (gradeSearched == arrayUsed[mid]){ //if the grade searched is the midpoint, you're done! 
      System.out.println( "The grade " + gradeSearched + " was found using " + iterations + " iterations.");
      found = 1; 
       
      break; 
    }
    else { //otherwise, the midpoint is now the low 
      low = mid+1;
    }
    iterations ++; //count the iterations!
   
  }
  if(found == 0){ //if the grade wasd never found, tell the user 
    System.out.println( " The grade " + gradeSearched + " was not found. ");
  }
  }
  
  public static int[] randomize(int[] arrayUsed){  //this method will rearrange the array 
  int temp = 0;
  int ranNum = 0;
  for(int i=0; i<30; i++){    //switch a random element of the array with the first element of the array 30 times 
    ranNum = (int)(Math.random()*(14)) + 1; 
    temp = arrayUsed[0];
    arrayUsed[0] = arrayUsed[ranNum];
    arrayUsed[ranNum] = temp;
  
  }
  return arrayUsed;  //return the new array 
  }
  
  public static void print(int[] arrayUsed){
  for(int i = 0; i<arrayUsed.length; i++){ //while there are elements left in the array, keep printing out the next element
    System.out.println(arrayUsed[i] + " ");
  }
  }
  
  public static void linearSearch(int[] arrayUsed){ //the method for linear search 
    int gradeSearched = 0;
    Scanner scanner2 = new Scanner( System.in );
    System.out.print(" Enter a grade to be searched for. "); //ask the user for a grade to be searched for 
    gradeSearched = scanner2.nextInt();
    int i = 0;
    int j = 0;
    while(gradeSearched != arrayUsed[i]){ //search through every element in the array until the grade searched for is found 
      i++;
      j++; //count the iterations
    } 
    if(gradeSearched != arrayUsed[i]){
      System.out.println(" The grade " + gradeSearched + " was not found."); //if by the end of the array the grade was not found, tell the user
    }
    else if( gradeSearched == arrayUsed[i]){ //if the grade searched for was found, trll the user and tell them how many iterations 
      System.out.println("The grade " + gradeSearched + " was found using " + j + " iterations.");
    }
  }
  
  
  
  


}


