//Emily Baker CSE2 12/6/2018
//This program will sort an integer array from least to greatest, and count the number of iterations it takes to do so 
import java.util.Arrays;
public class InsertionSortLab10{
  public static void main(String[] args){
    int[] myArrayBest = {1, 2, 3, 4, 5, 6, 7, 8, 9}; //initialize and assign the arrays we will use 
    int[] myArrayWorst = {9, 8, 7, 6, 5, 4, 3, 2, 1};
    int iterBest = insertionSort(myArrayBest); //initialize the variables that will hold how many iterations have been taking to sort the array 
    int iterWorst = insertionSort(myArrayWorst);
    System.out.println("The total number of operations performed on the sorted array: " + iterBest); //print out the number of iterations for each array 
    System.out.println("The total number of operations performed on the reverse sorted array: " + iterWorst);
  }
  
  //the method for sorting the numbers
  public static int insertionSort(int[] list){
    System.out.println(Arrays.toString(list)); //prints out the initial array 
    int iterations = 0; //intitialize the iterations counter
    for(int i = 1; i<list.length; i++){ //go through every element in the array 
      iterations++; //update the iterations counter
      for(int j = i; j > 0; j--){ //begin at element i and to the left 
        if(list[j] < list[j-1]){  //if an element is less than the one before it, switch them 
          int temp = list[j];
          list [j] = list[j-1];
          list[j-1] = temp; 
          iterations++; //count the number of iterations taken 
        }
        else{
          
          break; //break out of the lupe if that part of the array is in order 
        }
      }
    System.out.println(Arrays.toString(list)); //print out the current array 
    }
  return iterations; //return the number of iterations taken to sort the array 
  }


}