//Emily Baker CSE2 12/6/2018
//this code will sort an inputed array from least to greatest and count the number of iterations it takes to do so 
import java.util.Arrays;
public class SelectionSortLab10{
  public static void main(String[] args){
    int[] myArrayBest = {1, 2, 3, 4, 5, 6, 7, 8, 9}; //initialize and assign the arrays myArrayBest and myArrayWorst
    int[] myArrayWorst = {9, 8 , 7, 6, 5, 4, 3, 2, 1};
    int iterBest = selectionSort(myArrayBest); //assign the variables that will holds the number of iterations for both arrays 
    int iterWorst = selectionSort(myArrayWorst);
    System.out.println("The total number of operations performed on the sorted array: " + iterBest); //print out the number of iterations performed for both arrays 
    System.out.println("The total number of operations performed on the resverse sorted array: " + iterWorst);
   }
  
  //the method for sorting the numbers
  public static int selectionSort(int[] list){
    System.out.println(Arrays.toString(list)); //prints the initial array 
    int iterations = 0; //intialize counter for iterations
    for(int i = 0; i<list.length - 1; i ++){ //repeat this for every element in the array 
      iterations ++; //update the iterations countet
      int currentMin = list[i]; //intialize the variables for the value and location of the minimum 
      int currentMinIndex = i;
      for(int j = i + 1; j < list.length; j++){ //compare every element in the array to the current minimum; if the element is smaller than the current min, make that the current min 
        if(list[j] < currentMin){
          currentMinIndex = j;
          iterations++; //count the iterations 
        }
        else{
          iterations++; //otherwise, don't change the current min but count the iterations
        }
        if (currentMinIndex != 1){ //switch the current min with the first number in the array 
          int temp = list[i];
          list[i] = list[j];
          list[j] = temp; 
        }
      }
    System.out.println(Arrays.toString(list)); //print out the new array 
    }
  return iterations;  //return the number of iterations
  }
  
  
  
  
  
  
  
}