//Emily Baker CSE2 9/18/2018
//This program will ask the user for doubles that represent the number of acres of land affected by hurricane precipitation and how many inches of rain were dropped on average
//The program will then convert the quantity of rain into cubic miles
import java.util.Scanner;
// 
//
public class Convert{
    			// main method required for every Java program
   			public static void main(String[] args) {
          
          Scanner myScanner = new Scanner( System.in ); //the scanner allows the user to input info
          System.out.print("Enter the affected area in acres: "); //this prompts the user to enter the number of acres
          double acresAffected = myScanner.nextDouble(); //this creates a variable for the number of acres affected that the user imputs
          System.out.print("Enter the rainfall in the affected area: " ); //this prompts the user to input the number of inches of rain that were dropped
          double inchesFallen = myScanner.nextDouble(); //this creates a variable for the number of inches fallen that the user inputs
          double cubicMiles; //this is the variable for the amount of rain in cubic miles
          double inchesInAcre = 6272640; //this is the number of square inches in an acre. We will use this to convert from acres to square inches
          double cubicMilesInInch = 0.00000000000000393147; //this is the number of cubic miles in an inch. we will use this to convert from cubic inches to miles
          double inchesAffected; //this creates a variable for the number of inches affected
          double inchesOfRain; //this creates a variable for the square inches of rain 
          
          inchesAffected = acresAffected*inchesInAcre; //this calculates how many inches were affected
          inchesOfRain = inchesAffected*inchesFallen; //this. calculated the amount of rain in cubic inches
          cubicMiles = inchesOfRain*cubicMilesInInch; //this converts the amount of rain in cubic inches to cubic miles
          System.out.println(cubicMiles + " cubic miles"); 
       
        }
}