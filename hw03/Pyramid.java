//Emily Baker CSE2 9/18/2018
//This program will prompt the user for the dimensions of a pyramid and return the volume inside the pyramid
import java.util.Scanner;
// 
//
public class Pyramid{
    			// main method required for every Java program
   			public static void main(String[] args) {
          Scanner myScanner = new Scanner( System.in ); //the scanner allows the user to input info
          System.out.print("The square side of the pyramid is (input length): "); //this prompts the user to enter the lenth of the square side of the pyramid
          double squareSideLength = myScanner.nextDouble();; //this is the variable for the length of the square side that the user will input
          System.out.print("The height of pyramid is (input height): "); //this prompts the user to enter the height of the pyramid
          double height = myScanner.nextDouble(); //this is the variable for the height of the pyramid that the user will input
          double volume; //this is the variable for the volume of the pyramid that the program will compute
          double areaOfBase; //this is the variable for the area of the base of the pyramid
          areaOfBase = Math.pow(squareSideLength, 2.0); //this is the area of the base of the pyramid
          volume = areaOfBase*height/3; //this is the volume of the pyramid
          System.out.print("The volume inside the pyramid is: " + volume);
        }
}