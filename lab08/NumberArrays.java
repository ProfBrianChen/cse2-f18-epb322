import java.util.Scanner; //import the scanner

public class NumberArrays{ //class
public static void main(String[] args) { //main method
  int arrayLength = 100;  //establish the length of the arrays 
  int[] array1; //declare the first array
  array1 = new int [arrayLength]; //set the size of the array 
  int i;
  int g;
  for (i=0; i < arrayLength; i++){ //make each element of the array a random number 0-99 
    array1[i] = (int)(Math.random()*(100));;
  }
  
  
  int[] array2; //establish the second array 
  array2 = new int [arrayLength]; //set the length of the array 
  int j;
  int k;
  int counter; 
  for(j=0; j<arrayLength; j++){ //counts the number of each number in the first array, returns the number of n elements in the nth element of array2
    counter = 0; 
    for(k=0; k < arrayLength; k++){
      if (j==array1[k]){
        counter++;
      }
    array2[j] = counter; 
    }
  }
  
 for(g=0; g<arrayLength; g++){ //print out the number of times each number appears in array1
   if(array2[g] != 0){
     System.out.println( g + " occurs " + array2[g] + " times.");
   }
 }



}
}