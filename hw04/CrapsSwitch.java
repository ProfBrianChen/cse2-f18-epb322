//Emily Baker CSE2 9/25/18
//This program will either take 2 numbers that the user inputs or generate two random numbers
//The program will then determine the slang terminology associated with the numbers and print it out
import java.util.Scanner;
// 
//
public class CrapsSwitch{
    			// main method required for every Java program
   			public static void main(String[] args) {
          
          Scanner myScanner = new Scanner( System.in ); //the scanner allows the user to input info
          System.out.print("Would you like to randomly cast dice (enter '1') or state the two dice you want to evaluate (enter '2') ?"); // this prompts the user to choose how the numbers will be generated
          int userChoice = myScanner.nextInt(); //this creates a variable for the choice the user makes
          
          int num1 =0; //variable for first number
          int num2=0; //variable for second number
          
          String pre = ""; //variable for the "hard" or "easy"
          String slang = ""; //variable for the slang
         
          
          switch( userChoice ){ 
            case 2: //done if the user chooses to choose their own number
            System.out.print("Enter the first number 1-6 you'd like to use"); // this prompts the user to choose their first number
            num1 = myScanner.nextInt(); //assigns first number to num1
            System.out.print("Enter the second number 1-6 you'd like to use"); // this prompts the user to choose their second number 
            num2 = myScanner.nextInt(); //assigns second number to num2
            break;
            case 1: //done if the user chooses to have the numbers chosen randomly
            num1 = (int)(Math.random()*(6))+1; //generates two random numbers 1-6
            num2 = (int)(Math.random()*(6))+1;
          }
          int sum = num1 + num2; //sum of the two numbers, used to find the slang
          int hardEasy = num1%num2; //will be used to find out if certain rolls are hard or easy
          switch(hardEasy){ //if the numbers are the same (i.e. num1%num2=0) then they are a "hard" roll , otherwise they are easy
            case 0:
            pre = "Hard";
            case 1: 
            pre = "Easy";
            break;
            case 2:
            pre = "Easy";
            break;
            case 3:
            pre = "Easy";
            break;
            case 4: 
            pre = "Easy";
            break;
            case 5:
            pre = "Easy";
            break;
          }
          
          switch( sum ){ //the sum of the numbers correlate to the slang
            case 2:
            slang = "Snake Eyes";
            break;
            case 3:
            slang = "Ace Deuce";
            break;
            case 4:
            slang = pre + " Four";
            break;
            case 5:
            slang = "Fever Five";
            break;
            case 6:
            slang = pre + " Six";
            break;
            case 7:
            slang = "Seven Out";
            break;
            case 8:
            slang = pre + " Eight";
            break;
            case 9:
            slang = "Nine";
            case 10:
            slang = pre + "Ten";
            break;
            case 11:
            slang = "Yo-leven";
            break;
            case 12:
            slang = "Boxcars";
            break;
              
            }
         System.out.println("The numbers " + num1 + " and " + num2 + " produce the slang " + slang); //print out the numbers and slang
        
        
        }
}