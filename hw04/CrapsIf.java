//Emily Baker CSE2 9/25/18
//This program will either take 2 numbers that the user inputs or generate two random numbers
//The program will then determine the slang terminology associated with the numbers and print it out
import java.util.Scanner;
// 
//
public class CrapsIf{
    			// main method required for every Java program
   			public static void main(String[] args) {
          
          Scanner myScanner = new Scanner( System.in ); //the scanner allows the user to input info
          System.out.print("Would you like to randomly cast dice (enter '1') or state the two dice you want to evaluate (enter '2')? "); // this prompts the user to choose how the numbers will be generated
          double userChoice = myScanner.nextDouble(); //this creates a variable for the choice the user makes
          
          double num1 = 0; //establish the variables num1 and num2. These are the numbers that will be used to generate the saying
          double num2 = 0;
          String saying = ""; //establish the variable saying. This is what the slang will be stored under
          
          if(userChoice == 1){  //if the user chose to have the numbers generated randomly this statement will run
            num1 = (int)(Math.random()*(6))+1; //generate a random number between 1 and 6
            num2 = (int)(Math.random()*(6))+1; //generate a random number between 1 and 6
          }
          
          else if(userChoice == 2){ //this will run if the user decides to choose the numbers themselves
            System.out.print("Enter the first number 1-6 you'd like to use"); // this prompts the user to enter their first number
            num1 = myScanner.nextDouble(); //set number entered as num1
            System.out.print("Enter the second number 1-6 you'd like to use"); // this prompts the user to enter their second number
            num2 = myScanner.nextDouble(); //set number entered as num2
            }
          
          else{
            System.out.print("Please enter '1' or '2' "); //if the user doesnt enter a 1 or 2 the computer will let them know
          }
        
        if(num1 == 1 & num2 == 1){ //the if statements below take the two numbers and assign the value of saying to their slang
          saying = "Snake Eyes";
        }
        else if((num1 == 2 & num2 == 1) | (num2 == 2 & num1 == 1)){
          saying = "Ace Deuce";
        }
        else if(num1 == 2 & num2 == 2){
          saying = "Hard Four";
        }
        else if((num1 == 3 & num2 == 1) | (num2 == 3 & num1 == 1)){
          saying = "Easy Four";
        }
        else if((num1 == 3 & num2 == 2) | (num1 == 2 & num2 == 3)){
          saying = "Fever Five";
        }
        else if(num1 == 3 & num2 == 3){
          saying = "Hard Six";
        }
        else if((num1 == 4 & num2 == 1) | (num1 == 1 & num2 == 4)){
          saying = "Fever Five";
        }
        else if((num1 == 4 & num2 == 2) | (num1 == 2 & num2 == 4)){
          saying = "Easy Six";
        }
        else if((num1 == 4 & num2 == 3) | (num1 == 3 & num2 == 4)){
          saying = "Seven Out";
        }
        else if(num1 == 4 & num2 ==4){
          saying = "Hard Eight";
        }
        else if((num1 == 5 & num2 == 1) | (num1 == 1 & num2 == 5)){
          saying = "Easy Six";
        }
        else if((num1 == 5 & num2 == 2) | (num1 == 2 & num2 == 5)){
          saying = "Seven Out";
        }
        else if((num1 == 5 & num2 == 3) | (num1 == 3 & num2 == 5)){
          saying = "Easy Eight";
        }
        else if((num1 == 5 & num2 == 4) | (num1 == 4 & num2 == 5)){
          saying = "Nine";
        }
        else if(num1 == 5 & num2 == 5){
          saying = "Hard Ten";
        }
        else if((num1 == 6 & num2 == 1) | (num1 == 1 & num2 == 6)){
          saying = "Seven Out";
        }
        else if((num1 == 6 & num2 == 2) | (num1 == 2 & num2 == 6)){
          saying = "Easy Eight";
        }
        else if((num1 == 6 & num2 == 3) | (num1 == 3 & num2 == 6)){
          saying = "Nine";
        }
        else if((num1 == 6 & num2 == 4) | (num1 == 4 & num2 == 6)){
          saying = "Easy Ten";
        }
        else if((num1 == 6 & num2 == 5) | (num1 == 5 & num2 == 6)){
          saying = "Yo-leven";
        }
        else if(num1 == 6 & num2 == 6){
          saying = "Boxcars";
        }
        
          
      System.out.println("The numbers " + num1 + " and " + num2 + " produce the slang " + saying); //print out the numbers and their slang
        
          
        
        }
}
          
         
            